package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    /** Klassen CellGrid inneholder fem tomme medtoder og en tom konstruktør. 
     * For å implementere disse metodene riktig trenger CellGrid noen feltvariabler: 
     * en bredde-verdi (cols), en høyde-verdi (rows) og en datastruktur (CellState[][]) 
     * som holder på rows*cols antall CellState-verdier. 
     * Legg inn feltvariablene som mangler i CellGrid og assign verdier til dem i CellGrid-konstruktøren. 
     * Listen skal fylles med den CellState-verdien som blir gitt til konstruktøren. (add-commit-push)
     * Nå skal koden kompilere, og testene kjøre (men faile). Da kan vi begynne å implementere metodene.
     * Det er ofte greit å starte fra de enkleste metodene: her er det numRows og numColumns. 
     * Sjekk om det er tester for dem; i så fall kan du bruke testene sammen med dokumentasjonen 
    * i IGrid til å se hvordan de skal fungere.*/
    /** Bredde-verdi */
    private int cols;
    /** Høyde-verdi */
    private int rows;
    /** Cellstate */
    private CellState[][] grid;
    //private datastructure = new CellState [rows] [columns];

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.cols = columns;

        this.grid = new CellState[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                grid[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid newGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                newGrid.set(i, j, get(i, j));
            }
        }
        return newGrid;
    }
    
}
