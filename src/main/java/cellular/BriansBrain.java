package cellular;


import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{
    IGrid currentGeneration;

	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else if (random.nextBoolean()){
					currentGeneration.set(row, col, CellState.DYING);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return this.currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return this.currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return this.currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row < this.numberOfRows(); row++) {
			for (int col = 0; col < this.numberOfColumns(); col++) {
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState cellCurrent = getCellState(row, col);
		int neighbors = this.countNeighbors(row, col, CellState.ALIVE);

        if (cellCurrent.equals(CellState.ALIVE)) {
            return CellState.DYING;
        }
        else if (cellCurrent.equals(CellState.DYING)) {
			return CellState.DEAD;
		}
		else if (cellCurrent.equals(CellState.DEAD) && neighbors == 2) {
			return CellState.ALIVE;
		}
		else {
			return CellState.DEAD;
		}
		

	}
	private int countNeighbors(int row, int col, CellState state) {
		int counter = 0;
		for (int i = row - 1; i <= row + 1; i++) {
			for (int j = col - 1; j <= col + 1; j++) {
				if (i ==row && j == col) {
					continue;   // Hvis det er samme celle hopper vi over
				}
				if (i < 0) {
					continue;  // Utenfor brettet
				}
				if (j < 0) {
					continue;  // Utenfor brettet
				}
				if (i >= currentGeneration.numRows()) {
					continue;  // Utenfor brettet
				}
				if (j >= currentGeneration.numColumns()) {
					continue;
				}

				if (currentGeneration.get(i, j) == CellState.ALIVE) {
					counter ++;
				}
			}
		}
		return counter;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
